'use strict';

/**
 * @ngdoc overview
 * @name hardcoreProduceApp
 * @description
 * # hardcoreProduceApp
 *
 * Main module of the application.
 */
angular
  .module('hardcoreProduceApp', [
    'ngAnimate',
    'ngRoute'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
