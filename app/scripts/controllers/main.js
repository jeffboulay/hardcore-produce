'use strict';

/**
 * @ngdoc function
 * @name hardcoreProduceApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the hardcoreProduceApp
 */
angular.module('hardcoreProduceApp')
  .controller('MainCtrl', function ($scope,$timeout,$q,produceFactory) {
		var poller = new produceFactory.Poller();
		$scope.leaderboard = [];
		var fruits=[],
			veggies=[],
			timeout = 300;
		function getFruits(data){fruits = data;}
		function getVeggies(data){veggies = data;}
		function setProduce(data){
			if(data.length > 0){
			angular.copy(data, $scope.leaderboard);
				timeout=10000;
			}
		}

		function poll() {
			$timeout(function() {
				$q.all(
					poller.poll({type: 'fruits',limit: 10},getFruits),
					poller.poll({type: 'veggies',limit: 10},getVeggies)
				).then(
					setProduce(fruits.concat(veggies))
				);
				poll();
			}, timeout);
		}
		poll();
  });
